﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace centrvd.HelpDesk.Server
{
  public class ModuleFunctions
  {

    /// <summary>
    /// 
    /// </summary>
    [Public]
    public static string CalculateDayInWork(DateTime createDate, DateTime closedDate)
    {
      return createDate.Subtract(closedDate).Days.ToString();
    }

    /// <summary>
    /// Создать внешнее обращение.
    /// </summary>
    /// <returns>Созданное внешнее обращение.</returns>
    [Remote]
    public static IExternalRequest CreateExternalRequest()
    {
      // Создать внешнее обращение.
      return ExternalRequests.Create();
    }

    /// <summary>
    /// Создать внутреннее обращение.
    /// </summary>
    /// <returns>Созданное внутреннее обращение.</returns>
    [Remote]
    public static IInternalRequest CreateInternalRequest()
    {
      // Создать внутреннее обращение.
      return InternalRequests.Create();
    }

  }
}