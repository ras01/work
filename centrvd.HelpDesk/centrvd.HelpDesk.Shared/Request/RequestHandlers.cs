﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.HelpDesk.Request;

namespace centrvd.HelpDesk
{
  partial class RequestSharedHandlers
  {

    public virtual void RequestKindChanged(centrvd.HelpDesk.Shared.RequestRequestKindChangedEventArgs e)
    {
      Functions.Request.FillName(_obj);       
    }

    public virtual void CreatedDateChanged(Sungero.Domain.Shared.DateTimePropertyChangedEventArgs e)
    {
      Functions.Request.FillName(_obj);      
    }

    public virtual void DescriptionChanged(Sungero.Domain.Shared.StringPropertyChangedEventArgs e)
    {
      Functions.Request.FillName(_obj);      
    }

    public virtual void NumberChanged(Sungero.Domain.Shared.IntegerPropertyChangedEventArgs e)
    {
      Functions.Request.FillName(_obj);
    }

    public virtual void CollectionPropertyChanged(Sungero.Domain.Shared.CollectionPropertyChangedEventArgs e)
    {
      _obj.SumOfHours=_obj.CollectionProperty.Sum(h=>h.CountOfHours);
    }
  }

  partial class RequestCollectionPropertySharedCollectionHandlers
  {

    public virtual void CollectionPropertyAdded(Sungero.Domain.Shared.CollectionPropertyAddedEventArgs e)
    {
      _added.Employee=Sungero.Company.Employees.Current;
    }
  }
}