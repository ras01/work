﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.TenderSolution.Company;

namespace centrvd.TenderSolution.Client
{
  partial class CompanyActions
  {
    public virtual void Tenders(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var Documents = Functions.Company.Remote.GetCommitteeDecisions(_obj);
      if (Documents.Any())
        Documents.Show();
      else
        Dialogs.NotifyMessage("Не найдены тендеры организации");
    }

    public virtual bool CanTenders(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      var GeneralManager=Roles.GetAll().Where(n=>n.Sid==centrvd.TenderSolution.Constants.Module.RoleGUID.GeneralManager).FirstOrDefault();
      var TendersResponsible=Roles.GetAll().Where(n=>n.Sid==centrvd.TenderSolution.Constants.Module.RoleGUID.TendersResponsible).FirstOrDefault();
      bool gm=GeneralManager.RecipientLinks.Any(t=>t.Member==Sungero.Company.Employees.Current);
      bool tr=TendersResponsible.RecipientLinks.Any(t=>t.Member==Sungero.Company.Employees.Current);
      bool dep=centrvd.Tender.CommitteeDecisions.GetAll().Any(t=>t.Department==Sungero.Company.Employees.Current.Department);
      if(gm||tr||dep)
        return true;
      else
        return false;
    }

  }

}