﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.HelpDesk.Request;

namespace centrvd.HelpDesk
{
  partial class RequestFilteringClientHandler
  {

    public override void ValidateFilterPanel(Sungero.Domain.Client.ValidateFilterPanelEventArgs e)
    {
      
    }
  }

  partial class RequestClientHandlers
  {

    public override void Refresh(Sungero.Presentation.FormRefreshEventArgs e)
    {
      if(_obj.LifeCycle==LifeCycle.Closed)
        _obj.State.IsEnabled=false;
    }

  }
}