﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace centrvd.ResourceManagment.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      CreateRole();
      GrantRights();
    }
    /// <summary>
    /// Создание роли
    /// </summary>
    public static void CreateRole()
    {
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateRole("Администраторы ВМ",
                                                                      "Имеют полный доступ к ВМ, запросам + просмотр/изменение настроек модуля",
                                                                      Constants.Module.RoleGUID.VMAdmin);
    }
    
    /// <summary>
    /// Выдача прав
    /// </summary>
    public static void GrantRights()
    {
      var allUsers = Roles.GetAll().Where(n => n.Sid  == Constants.Module.RoleGUID.allUsers).FirstOrDefault();
      ModuleSettingses.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
      VirtualMachines.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Read);
      VMRequests.AccessRights.Grant(allUsers, DefaultAccessRightsTypes.Change);
      ModuleSettingses.AccessRights.Save();
      VirtualMachines.AccessRights.Save();
      VMRequests.AccessRights.Save();
      
      var vmAdmin = Roles.GetAll().Where(n => n.Sid == Constants.Module.RoleGUID.VMAdmin).FirstOrDefault();
      VirtualMachines.AccessRights.Grant(vmAdmin, DefaultAccessRightsTypes.FullAccess);
      VMRequests.AccessRights.Grant(vmAdmin, DefaultAccessRightsTypes.FullAccess);
      ModuleSettingses.AccessRights.Grant(vmAdmin, DefaultAccessRightsTypes.Change);
        
      VirtualMachines.AccessRights.Save();
      VMRequests.AccessRights.Save();
      ModuleSettingses.AccessRights.Save();
    }
  }

}
