﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.ResourceManagment.VirtualMachine;

namespace centrvd.ResourceManagment
{
  partial class VirtualMachineFilteringServerHandler<T>
  {

    public override IQueryable<T> Filtering(IQueryable<T> query, Sungero.Domain.FilteringEventArgs e)
    {
      var VMAdmin = Roles.GetAll().Where(n =>n.Sid == Constants.Module.RoleGUID.VMAdmin).FirstOrDefault();

      if(Users.Current.IncludedIn(Roles.Administrators) ||
         (Sungero.Company.Employees.Current != null &&
          Sungero.Company.Employees.Current.IncludedIn(VMAdmin)))
        return query;
      
      var projectManager=Roles.GetAll().Where(n => n.Sid == Constants.Module.RoleGUID.projecManager).FirstOrDefault();
      if(Sungero.Company.Employees.Current.IncludedIn(projectManager))
        query=query.Where(r => r.Project.Manager == Sungero.Company.Employees.Current ||
                          r.Responsible == null);
      else
        query=query.Where(r => r.Responsible == Sungero.Company.Employees.Current ||
                          r.UsersCollection.Any(t => t.User == Sungero.Company.Employees.Current) ||
                          r.Responsible == null);
      
      return query;
    }
  }

  partial class VirtualMachineServerHandlers
  {

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      _obj.VMCycle = VirtualMachine.VMCycle.Created;
      _obj.LastCheckDate = Calendar.Now;
      _obj.Status = VirtualMachine.Status.Active;
    }
  }

}