﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.Tender.CommitteeDecision;

namespace centrvd.Tender.Client
{
  partial class CommitteeDecisionCollectionActions
  {
    public override void CopyToClipboard(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.CopyToClipboard(e);
    }

    public override bool CanCopyToClipboard(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanCopyToClipboard(e);
    }

  }


  partial class CommitteeDecisionActions
  {
    public override void CopyToClipboardFromCard(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      base.CopyToClipboardFromCard(e);
    }

    public override bool CanCopyToClipboardFromCard(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return base.CanCopyToClipboardFromCard(e);
    }

    public virtual void DocumentsByDecision(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var Documents=DocumentByTCDecisions.GetAll(r=>r.TCDecision==_obj);
      Documents.Show();
    }

    public virtual bool CanDocumentsByDecision(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

  }

}