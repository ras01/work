﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace centrvd.ResourceManagment.Client
{
  public class ModuleFunctions
  {

    /// <summary>
    /// Создать и отобразить карточку справочника «Запросы на предоставление ВМ».
    /// </summary>
    public virtual void CreateVMRequests()
    {
      Functions.Module.Remote.CreateIVMRequest().Show();
    }
    
    /// <summary>
    /// Создать карточку справочника «Виртуальная машина» из CSV.
    /// </summary>
    public virtual void CreateVMCSV()
    {
      var dFile=Dialogs.CreateInputDialog("Выберите файл");
      var fileSelect = dFile.AddFileSelect("Файл импорта данных", true);
      fileSelect.WithFilter("CSV файлы", "*.csv*","csv");
      if (dFile.Show() == DialogButtons.Ok)
      {
        string tempPath = System.IO.Path.GetTempPath();
        string fileName = string.Format(@"{0}{1}",tempPath,fileSelect.Value.Name);
        File.WriteAllBytes(fileName, fileSelect.Value.Content);
        Functions.Module.Remote.CreateVMFromCSV(fileName);
      }

    }

  }
}