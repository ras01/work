﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.HelpDesk.ExternalRequest;

namespace centrvd.HelpDesk
{
  partial class ExternalRequestSharedHandlers
  {

    public virtual void ContactChanged(centrvd.HelpDesk.Shared.ExternalRequestContactChangedEventArgs e)
    {
      if(_obj.Company==null)
        _obj.Company=_obj.Contact.Company;
    }

    public virtual void CompanyChanged(centrvd.HelpDesk.Shared.ExternalRequestCompanyChangedEventArgs e)
    {
      if(_obj.Contact!=null && _obj.Contact.Company!=_obj.Company)
        _obj.Contact=null;
    }

  }
}