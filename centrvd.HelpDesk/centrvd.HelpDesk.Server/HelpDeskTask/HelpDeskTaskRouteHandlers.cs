﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using centrvd.HelpDesk.HelpDeskTask;

namespace centrvd.HelpDesk.Server
{
  partial class HelpDeskTaskRouteHandlers
  {

    public virtual void CompleteAssignment5(centrvd.HelpDesk.IReviewRequestAssignment assignment, centrvd.HelpDesk.Server.ReviewRequestAssignmentArguments e)
    {
      var request=_obj.AttachmentGroupRequest.Requests.Single();
      if(assignment.Result == centrvd.HelpDesk.ReviewRequestAssignment.Result.Complete)
      {
        request.Result=_obj.ActiveText;
        request.LifeCycle=centrvd.HelpDesk.Request.LifeCycle.Closed;
      }
      if(assignment.Result == centrvd.HelpDesk.ReviewRequestAssignment.Result.Review)
      {
        request.LifeCycle=centrvd.HelpDesk.Request.LifeCycle.InWork;
      }

    }

    public virtual void StartBlock5(centrvd.HelpDesk.Server.ReviewRequestAssignmentArguments e)
    {
      // Указать исполнителя задания.
      e.Block.Performers.Add(_obj.Author);

    }

    public virtual void StartBlock4(centrvd.HelpDesk.Server.RequestAssignmentArguments e)
    {
      // Указать исполнителем задания ответственного за обращение.
      var request = _obj.AttachmentGroupRequest.Requests.Single();
      e.Block.Performers.Add(request.Responsible);
    }

    public virtual void CompleteAssignment4(centrvd.HelpDesk.IRequestAssignment assignment, centrvd.HelpDesk.Server.RequestAssignmentArguments e)
    {
      // Указать нового ответственного в карточке обращения,
      // если задание выполнено с результатом "Переадресовано".
      if (assignment.Result == centrvd.HelpDesk.RequestAssignment.Result.Readdress)
        _obj.AttachmentGroupRequest.Requests.Single().Responsible = assignment.NewResponsible;
      
      if(assignment.Result == centrvd.HelpDesk.RequestAssignment.Result.Complete)
      {
        var request=_obj.AttachmentGroupRequest.Requests.Single();
        request.LifeCycle=centrvd.HelpDesk.Request.LifeCycle.InControl;
      }
    }

    public virtual void Script3Execute()
    {
      // Создать внутреннее обращение.
      var request = InternalRequests.Create();
      // Заполнить карточку обращения.
      request.Description = _obj.ActiveText.Substring(0, _obj.ActiveText.Length >= 150
                                                      ? 150 : _obj.ActiveText.Length);
      request.Author =Sungero.Company.Employees.As(_obj.Author);
      request.RequestKind = _obj.RequestKind;
      request.Name = _obj.RequestKind + ":" + _obj.ActiveText.Substring(0,
                                                                     _obj.ActiveText.Length >= 50 ? 50 : _obj.Subject.Length);
      // Вычисление ответственного за решение обращения:
      var SDrole=Roles.GetAll(r=>r.Name=="Дежурный сотрудник службы поддержки");
      var users=Roles.GetAllUsersInGroup(SDrole.First());
      request.Responsible = Sungero.Company.Employees.As(users.First());
      request.LifeCycle = centrvd.HelpDesk.Request.LifeCycle.InWork;
      // Вложить созданное обращение в задачу.
      _obj.AttachmentGroupRequest.Requests.Add(request);
      _obj.Subject=string.Format("Обращение {0}. {1}",request.Number, request.Name);

    }

  }
}