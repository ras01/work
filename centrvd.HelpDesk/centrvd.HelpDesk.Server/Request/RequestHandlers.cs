﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Company;
using centrvd.HelpDesk.Request;

namespace centrvd.HelpDesk
{
  partial class RequestFilteringServerHandler<T>
  {

    public override IQueryable<T> Filtering(IQueryable<T> query, Sungero.Domain.FilteringEventArgs e)
    {
      // Проверка того, что панель фильтрации включена.
      if (_filter == null)
        return query;
      // Контрол "Набор флажков". Фильтр по состоянию обращения.
      // Возможные значения: В работе, На контроле, Закрыто.
      if (_filter.FlagInWork || _filter.FlagInControl || _filter.FlagClosed)
        query = query.Where(r => (((_filter.FlagInWork &&
                                    r.LifeCycle.Equals(Request.LifeCycle.InWork)) ||
                                   (_filter.FlagInControl &&
                                    r.LifeCycle.Equals(Request.LifeCycle.InControl)) ||
                                   (_filter.FlagClosed &&
                                    r.LifeCycle.Equals(Request.LifeCycle.Closed))) &&
                                  ((_filter.InternalRequests &&
                                    InternalRequests.Is(r)) ||
                                   (_filter.ExternalRequests &&
                                    ExternalRequests.Is(r))) &&
                                  ((_filter.Employee!=null &&
                                    r.Responsible.Equals(_filter.Employee)) ||
                                   (_filter.Im&&
                                    r.Responsible.Equals(Employees.Current) ||
                                    (_filter.All && _filter.Employee==null &&
                                     r.Id!=null)
                                   ))));
      return query;
    }
  }

  partial class RequestServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      if(_obj.LifeCycle.Value==LifeCycle.Closed)
        _obj.ClosedDate=Calendar.Now;
      if(_obj.LifeCycle==LifeCycle.Closed && string.IsNullOrEmpty(_obj.Result))
        e.AddError("Перед закрытием обращения проверьте результат");
      Functions.Request.FillName(_obj);
    }

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      _obj.Number=_obj.Id;
      _obj.Responsible=Employees.Current;
      _obj.LifeCycle=Request.LifeCycle.InWork;
      _obj.CreatedDate=Calendar.Now;
      _obj.Name="Тема обращения будет сформирована автоматически";
    }
  }

}