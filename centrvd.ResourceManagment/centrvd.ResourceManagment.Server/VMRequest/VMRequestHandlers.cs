﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.ResourceManagment.VMRequest;

namespace centrvd.ResourceManagment
{
  partial class VMRequestFilteringServerHandler<T>
  {

    public override IQueryable<T> Filtering(IQueryable<T> query, Sungero.Domain.FilteringEventArgs e)
    {
      var VMAdmin = Roles.GetAll().Where(n => n.Sid == Constants.Module.RoleGUID.VMAdmin).FirstOrDefault();
      if(Sungero.Company.Employees.Current.IncludedIn(VMAdmin))
        return query;
            
      var projectManager = Roles.GetAll().Where(n => n.Sid == Constants.Module.RoleGUID.projecManager).FirstOrDefault();
      if(Sungero.Company.Employees.Current.IncludedIn(projectManager))
        query = query.Where(r => r.Project.Manager == Sungero.Company.Employees.Current);
      else
        query = query.Where(r => r.Responsible == Sungero.Company.Employees.Current ||
                          r.UsersCollection.Any(t => t.User==Sungero.Company.Employees.Current));
         
      return query;
    }
  }

  partial class VMRequestServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      _obj.Name = "Запрос № " + _obj.Id + " от " + _obj.Responsible.Name + " по " + _obj.Project.Name; 
    }

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      _obj.Responsible = Sungero.Company.Employees.Current;
      _obj.RequestCycle = centrvd.ResourceManagment.VMRequest.RequestCycle.Created;
      _obj.CreateDate = Calendar.Now;
      _obj.Name = Constants.VMRequest.initName;
      _obj.InstallRX = false;
      _obj.BoolVPN = false;
    }
  }

}