﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.ResourceManagment.VMRequest;

namespace centrvd.ResourceManagment
{
  partial class VMRequestClientHandlers
  {

    public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
    {
      var actTasks = VMActualizationTasks.GetAll().Where(t => t.Status != VMActualizationTask.Status.Suspended &&
                                                         t.Attachments.Contains(_obj));
      if(actTasks.FirstOrDefault() != null)
        _obj.State.IsEnabled = false;
    }

    public virtual void RepositoryValueInput(Sungero.Presentation.EnumerationValueInputEventArgs e)
    {
      if(!_obj.Repository.Equals(VMRequest.Repository.Existing))
      {
        _obj.State.Properties.WorkRep.IsVisible = true;
        _obj.State.Properties.BaseRep.IsVisible = true;
      }
      else
      {
        _obj.State.Properties.WorkRep.IsVisible = false;
        _obj.State.Properties.BaseRep.IsVisible = false;
      }
    }

    public virtual void InstallRXValueInput(Sungero.Presentation.BooleanValueInputEventArgs e)
    {
      if(_obj.InstallRX==false)
      {
        _obj.State.Properties.RXVersion.IsVisible = true;
        _obj.State.Properties.Database.IsVisible = true;
        _obj.State.Properties.Repository.IsVisible = true;
      }
      else
      {
        _obj.State.Properties.RXVersion.IsVisible = false;
        _obj.State.Properties.Database.IsVisible = false;
        _obj.State.Properties.Repository.IsVisible = false;
        _obj.State.Properties.WorkRep.IsVisible = false;
        _obj.State.Properties.BaseRep.IsVisible = false;
      }
    }

  }
}