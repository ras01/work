﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.HelpDesk.ExternalRequest;

namespace centrvd.HelpDesk
{
  partial class ExternalRequestContactPropertyFilteringServerHandler<T>
  {

    public virtual IQueryable<T> ContactFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
    {
      // Фильтровать контакты по компании
      if (_obj.Company != null)
        query = query.Where(settlement => Equals(settlement.Company, _obj.Company));
      return query;
    }
  }





}