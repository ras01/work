﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using centrvd.ResourceManagment.VMActualizationTask;

namespace centrvd.ResourceManagment.Server
{
  partial class VMActualizationTaskRouteHandlers
  {

    public virtual bool Monitoring7Result()
    {
      var vm=_obj.AttachmentGroup.VirtualMachines.Single();
      if(vm.LastCheckDate.Value.AddDays(30).Equals(Calendar.Now))
        return true;
      return false;
    }

    public virtual void StartBlock6(Sungero.Workflow.Server.NoticeArguments e)
    {
      var vm=_obj.AttachmentGroup.VirtualMachines.Single();
      e.Block.Performers.Add(vm.Responsible);
      foreach(var user in vm.UsersCollection)
        e.Block.Performers.Add(user.User);
    }

    public virtual void CompleteAssignment3(centrvd.ResourceManagment.IArchiveAssignment assignment, centrvd.ResourceManagment.Server.ArchiveAssignmentArguments e)
    {
      var vm=_obj.AttachmentGroup.VirtualMachines.Single();
      vm.VMCycle=centrvd.ResourceManagment.VirtualMachine.VMCycle.Archive;
      vm.LastCheckDate=Calendar.Now;
    }

    public virtual void StartBlock3(centrvd.ResourceManagment.Server.ArchiveAssignmentArguments e)
    {
      var vmAdmin=Roles.GetAll().Where(n=>n.Sid==Constants.Module.RoleGUID.VMAdmin).FirstOrDefault();
      e.Block.Performers.Add(vmAdmin);
    }

    public virtual void CompleteAssignment4(centrvd.ResourceManagment.IDeleteAssignment assignment, centrvd.ResourceManagment.Server.DeleteAssignmentArguments e)
    {
      var vm=_obj.AttachmentGroup.VirtualMachines.Single();
      vm.VMCycle=centrvd.ResourceManagment.VirtualMachine.VMCycle.Deleted;
    }

    public virtual void StartBlock4(centrvd.ResourceManagment.Server.DeleteAssignmentArguments e)
    {
      var vm=_obj.AttachmentGroup.VirtualMachines.Single();
      vm.VMCycle=centrvd.ResourceManagment.VirtualMachine.VMCycle.OnDelete;
      var vmAdmin=Roles.GetAll().Where(n=>n.Sid==Constants.Module.RoleGUID.VMAdmin).FirstOrDefault();
      e.Block.Performers.Add(vmAdmin);
    }

    public virtual void StartBlock5(centrvd.ResourceManagment.Server.ActualizationAssignmentArguments e)
    {
      var vm=_obj.AttachmentGroup.VirtualMachines.Single();
      e.Block.Performers.Add(vm.Responsible);
    }

    public virtual void CompleteAssignment5(centrvd.ResourceManagment.IActualizationAssignment assignment, centrvd.ResourceManagment.Server.ActualizationAssignmentArguments e)
    {
      if(assignment.Result.Equals(centrvd.ResourceManagment.ActualizationAssignment.Result.Complete))
        _obj.AttachmentGroup.VirtualMachines.Single().LastCheckDate=Calendar.Now;
      if(assignment.Result.Equals(centrvd.ResourceManagment.ActualizationAssignment.Result.Delete))
      { 
        _obj.AttachmentGroup.VirtualMachines.Single().LastCheckDate=Calendar.Now;
        _obj.AttachmentGroup.VirtualMachines.Single().VMCycle=centrvd.ResourceManagment.VirtualMachine.VMCycle.OnDelete;
      }
      if(assignment.Result.Equals(centrvd.ResourceManagment.ActualizationAssignment.Status.Suspended))
      { 
        _obj.AttachmentGroup.VirtualMachines.Single().LastCheckDate=Calendar.Now;
        _obj.AttachmentGroup.VirtualMachines.Single().VMCycle=centrvd.ResourceManagment.VirtualMachine.VMCycle.Archive;
      }
    }

  }
}