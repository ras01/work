﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace centrvd.ResourceManagment.Structures.Module
{
  //Класс для получения данных из CVS файла
  partial class CSVData
  {
    public string Name { get; set; }
    public string Host { get; set; }
    public int UsedSpace { get; set; }
    public string GuestOS { get; set; }
    public string IP { get; set; }
    public string DNS { get; set; }
  }
}