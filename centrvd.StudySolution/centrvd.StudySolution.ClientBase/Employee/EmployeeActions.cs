﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.StudySolution.Employee;

namespace centrvd.StudySolution.Client
{
  partial class EmployeeActions
  {
    public virtual void EmployeeRequests(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var requests = Functions.Employee.Remote.GetEmployeeRequest(_obj);
      if (requests.Any())
        requests.Show();
      else
        Dialogs.NotifyMessage("У сотрудника нет обращений");

    }

    public virtual bool CanEmployeeRequests(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return true;
    }

  }

}