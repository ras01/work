﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace centrvd.HelpDesk
{
  partial class RequestReportClientHandlers
  {

    public override void BeforeExecute(Sungero.Reporting.Client.BeforeExecuteEventArgs e)
    {
      // Создать диалог с запросом периода дат.
      var dialog = Dialogs.CreateInputDialog("Параметры отчета");
      var startDate = dialog.AddDate("Дата от", true , Calendar.Today.AddDays(-30));
      var endDate = dialog.AddDate("Дата по", true, Calendar.Today);

      if (dialog.Show() != DialogButtons.Ok)
        e.Cancel = true;

      // Передать указанные значения в параметры отчета.
      RequestReport.startDate = startDate.Value.Value;
      RequestReport.endDate = endDate.Value.Value;
    }

  }
}