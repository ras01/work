﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.HelpDesk.InternalRequest;

namespace centrvd.HelpDesk.Server
{
  partial class InternalRequestFunctions
  {

    /// <summary>
    /// Получить все обращения сотрудника, являющегося автором текущего обращения.
    /// </summary>
    /// <returns>Список обращений.</returns>
    [Remote(IsPure=true)]
    public IQueryable<IInternalRequest> GetEmployeeRequests()
    {
      return InternalRequests.GetAll(r=>Equals(r.Author,_obj.Author));
    }

  }
}