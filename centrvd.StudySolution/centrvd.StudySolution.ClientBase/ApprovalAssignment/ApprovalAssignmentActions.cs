﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.StudySolution.ApprovalAssignment;

namespace centrvd.StudySolution.Client
{
  partial class ApprovalAssignmentActions
  {
    public virtual void ApprovedRemark(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      if(_obj.ActiveText=="")
        e.AddError("Не заполнено примечание!");
      var action = new Sungero.Workflow.Client.ExecuteResultActionArgs(e.FormType,
                                                                       e.Entity, e.Action);
      base.Approved(action);
      _obj.ApprovedRemark=true;
      _obj.Complete(Result.Approved);
      e.CloseFormAfterAction = true;

    }

    public virtual bool CanApprovedRemark(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      if(!_obj.Status.Equals(ApprovalAssignment.Status.Completed))
        return true;
      else 
        return false;
    }

  }

}