﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace centrvd.Tender.Client
{
  public class ModuleFunctions
  {

    /// <summary>
    /// Создать и отобразить карточку тендера.
    /// </summary>
    public virtual void CreateCommitteeDecisions()
    {
      Functions.Module.Remote.CreateCommitteeDecision().Show();
    }
  }
}