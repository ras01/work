﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace centrvd.HelpDesk.Server
{
  public class ModuleJobs
  {

    /// <summary>
    /// 
    /// </summary>
    public virtual void SendNotificationAboutRequests()
      
    {
      // Получить созданные за неделю обращения.
      var newRequests = Requests.GetAll(r => r.CreatedDate.Value >
                                        Calendar.Today.AddWorkingDays(-5));
      // Получить исполнителя по задаче,
      var department=Sungero.Company.Departments.Get().Where(r=>r.Name=="Служба поддержки");
      var performer=department.First().Manager;
      // Создать и стартовать задачу.
      var task= Sungero.Workflow.SimpleTasks.CreateWithNotices
        ("Статистика по обращениям", performer);
      task.ActiveText = "Зарегистрировано обращений" +
        newRequests.Count();
      task.Start();
    }

  }
}