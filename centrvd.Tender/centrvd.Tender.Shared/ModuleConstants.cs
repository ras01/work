﻿using System;
using Sungero.Core;

namespace centrvd.Tender.Constants
{
  public static class Module
  {
    public static readonly Guid DocumentByDecisionKind = Guid.Parse("3C691838-A91B-4ABD-BBB8-5A98898D629B");
    
    public static class RoleGUID
    {
      public static readonly Guid GeneralManager = Guid.Parse("775E5552-A769-44D0-90A5-0A93457285A8");
      public static readonly Guid TendersResponsible = Guid.Parse("5DD65628-941B-491C-B93F-5B3BE276174A");
    }
  }
}