﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.ResourceManagment.VirtualMachine;

namespace centrvd.ResourceManagment
{
  partial class VirtualMachineSharedHandlers
  {

    public virtual void RequestChanged(centrvd.ResourceManagment.Shared.VirtualMachineRequestChangedEventArgs e)
    {
      if(_obj.Request!=null&&e.OldValue!=e.NewValue)
      {
        if(_obj.Project==null)
          _obj.Project=_obj.Request.Project;
        if(_obj.Responsible==null)
          _obj.Responsible=_obj.Request.Responsible;
        if(_obj.OS==null)
          _obj.OS=_obj.Request.OS;
        if(_obj.UsersCollection.Count==0)
        {
          var users=_obj.Request.UsersCollection;
          foreach(var reqUser in users)
          {
            var user=_obj.UsersCollection.AddNew();
            user.User=reqUser.User;
          }
        }
      }
      else
      {
        _obj.Project=null;
        _obj.Responsible=null;
        _obj.OS=null;
        _obj.UsersCollection.Clear();
      }
    }

    public virtual void VMCycleChanged(Sungero.Domain.Shared.EnumerationPropertyChangedEventArgs e)
    {
      if(_obj.VMCycle.Equals(VirtualMachine.VMCycle.Deleted))
        _obj.Status=VirtualMachine.Status.Closed;
    }

  }
}