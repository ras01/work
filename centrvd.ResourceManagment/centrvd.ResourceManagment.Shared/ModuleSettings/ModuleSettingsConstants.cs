﻿using System;
using Sungero.Core;

namespace centrvd.ResourceManagment.Constants
{
  public static class ModuleSettings
  {
    public const string Name="Настройки модуля Управление ресурсами";
    public const int FreqCheck=12;
    public const int TaskDeadlineAct=3;
    public const int TaskDeadlineGive=2;
  }
}