﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace centrvd.HelpDesk
{
  partial class RequestReportServerHandlers
  {

    public virtual IQueryable<centrvd.HelpDesk.IRequest> GetRequest()
    {
      //Получить обращения за период.
      return centrvd.HelpDesk.Requests.GetAll()
        .Where(a => a.CreatedDate >= RequestReport.startDate)
        .Where(a => a.CreatedDate <= RequestReport.endDate);
    }

  }
}