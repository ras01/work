﻿using System;
using Sungero.Core;

namespace centrvd.ResourceManagment.Constants
{
  public static class Module
  {
    public static class RoleGUID
    {
      public static readonly Guid VMAdmin=Guid.Parse("6D5C0E13-FA35-45CD-AFAD-82FFDEF2CCD2");
      public static readonly Guid projecManager=Guid.Parse("61016C45-E26C-4CF8-B4BE-09F191AC1BCA");
      public static readonly Guid allUsers=Guid.Parse("440103EA-A766-47A8-98AD-5260CA32DE46");
    }
  }
}