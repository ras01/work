﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.Tender.CommitteeDecision;

namespace centrvd.Tender
{
  partial class CommitteeDecisionReserveSupplierCollectionSharedHandlers
  {

    public virtual void ReserveSupplierCollectionReserveSupplierChanged(centrvd.Tender.Shared.CommitteeDecisionReserveSupplierCollectionReserveSupplierChangedEventArgs e)
    {
      _obj.TIN=_obj.ReserveSupplier.TIN;
    }
  }

  partial class CommitteeDecisionMainSupplierCollectionSharedHandlers
  {

    public virtual void MainSupplierCollectionMainSupplierChanged(centrvd.Tender.Shared.CommitteeDecisionMainSupplierCollectionMainSupplierChangedEventArgs e)
    {
      _obj.TIN=_obj.MainSupplier.TIN;
    }
  }

  partial class CommitteeDecisionSharedHandlers
  {

    public virtual void OperatesOnChanged(Sungero.Domain.Shared.DateTimePropertyChangedEventArgs e)
    {
      if(e.NewValue.Value.CompareTo(Calendar.Now)<0)
        _obj.Condition=CommitteeDecision.Condition.UnActive;
      else
        _obj.Condition=null;
    }

    public virtual void ValidityChanged(Sungero.Domain.Shared.EnumerationPropertyChangedEventArgs e)
    {
      if(_obj.BeginDate!=null)
      {
        if(e.NewValue.Equals(CommitteeDecision.Validity.Year))
          _obj.OperatesOn=_obj.BeginDate.Value.AddYears(1);
        if(e.NewValue.Equals(CommitteeDecision.Validity.TwoYears))
          _obj.OperatesOn=_obj.BeginDate.Value.AddYears(2);
        if(e.NewValue.Equals(CommitteeDecision.Validity.Quarter))
          _obj.OperatesOn=_obj.BeginDate.Value.AddMonths(3);
        
        if(e.NewValue.Equals(CommitteeDecision.Validity.Date))
          _obj.Notify=60;
        else
          _obj.Notify=null;
      }
    }

    public virtual void BeginDateChanged(Sungero.Domain.Shared.DateTimePropertyChangedEventArgs e)
    {
      if(_obj.Validity.Equals(CommitteeDecision.Validity.Year))
        _obj.OperatesOn=_obj.BeginDate.Value.AddYears(1);
      if(_obj.Validity.Equals(CommitteeDecision.Validity.TwoYears))
        _obj.OperatesOn=_obj.BeginDate.Value.AddYears(2);
      if(_obj.Validity.Equals(CommitteeDecision.Validity.Quarter))
        _obj.OperatesOn=_obj.BeginDate.Value.AddMonths(3);
    }

  }
}