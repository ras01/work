﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.HelpDesk.Request;

namespace centrvd.HelpDesk.Shared
{
  partial class RequestFunctions
  {

    /// <summary>
    /// Получить тему обращения
    /// </summary>
    /// <returns>Тема обращения</returns>
    [Public]
    public virtual void FillName()
    {
      string RequestType="", RequestNumber="", RequestDate="", RequestDescription="";
      if(_obj.RequestKind!=null)
        RequestType=_obj.RequestKind.DisplayValue.ToString();
      if(_obj.Number!=null)
        RequestNumber=_obj.Number.ToString();
      if(_obj.CreatedDate!=null)
        RequestDate=_obj.CreatedDate.Value.ToString("d", CultureInfo.GetCultureInfo("de-DE"));
      if(_obj.Description!=null)
      {
        int x= _obj.Description.Length>50 ? 50 : _obj.Description.Length;
        RequestDescription=_obj.Description.Substring(0,x);
      }
      _obj.Name=string.Format("{0} № {1} от {2}: {3}",RequestType, RequestNumber, RequestDate, RequestDescription);
    }

  }
}