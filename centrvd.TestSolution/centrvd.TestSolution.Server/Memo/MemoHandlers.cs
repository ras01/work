﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.TestSolution.Memo;

namespace centrvd.TestSolution
{
  partial class MemoServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      base.BeforeSave(e);
      var prefixName = Sungero.Docflow.Constants.OfficialDocument.RegistrationNumberPrefix;
      if (e.Params.Contains(prefixName))
      {
        var prefix = string.Empty;
        e.Params.TryGetValue(prefixName, out prefix);
        
        prefix = TestSolution.PublicFunctions.DocumentRegister.ChangeCustomElementNumber(_obj, prefix);
        e.Params.AddOrUpdate(prefixName, prefix);
      }
      
      var postfixName = Sungero.Docflow.Constants.OfficialDocument.RegistrationNumberPostfix;
      if (e.Params.Contains(postfixName))
      {
        var postfix = string.Empty;
        e.Params.TryGetValue(postfixName, out postfix);
        
        postfix = TestSolution.PublicFunctions.DocumentRegister.ChangeCustomElementNumber(_obj, postfix);
        e.Params.AddOrUpdate(postfixName, postfix);
      }
    }
  }

}