﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.StudySolution.ApprovalAssignment;

namespace centrvd.StudySolution
{
  partial class ApprovalAssignmentServerHandlers
  {

    public override void BeforeComplete(Sungero.Workflow.Server.BeforeCompleteEventArgs e)
    {
      if(_obj.ApprovedRemark!=null)
        e.Result=ApprovalAssignments.Resources.ApprovedRemark;
    }
  }

}