﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.ResourceManagment.VMRequest;

namespace centrvd.ResourceManagment.Client
{
  partial class VMRequestActions
  {
    public virtual void CreateVM(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var VM = VirtualMachines.Create();
      VM.Request = _obj;
      VM.Project = _obj.Project;
      VM.Responsible = _obj.Responsible;
      VM.ShowModal();
    }

    public virtual bool CanCreateVM(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      var VM = VirtualMachines.GetAll().Where(v => v.Request == _obj);
      if(VM.FirstOrDefault() != null)
        return false;
      return true;
    }


    public virtual void CreateRequest(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      _obj.Save();
      var vmAdmin = Roles.GetAll().Where(n => n.Sid == Constants.Module.RoleGUID.VMAdmin).FirstOrDefault();
      var task = Sungero.Workflow.SimpleTasks.Create(Constants.VMRequest.taskSubject,
                                                     Calendar.Now.AddDays(Convert.ToDouble(ModuleSettingses.GetAll().FirstOrDefault().TaskDeadlineGive)),
                                                     vmAdmin);
      
      task.Attachments.Add(_obj);
      task.Start();
    }

    public virtual bool CanCreateRequest(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      if(_obj.Responsible.Equals(Sungero.Company.Employees.Current))
        return true;
      var VMReqTasks = Sungero.Workflow.SimpleTasks.GetAll().Where(a => a.Attachments.Contains(_obj));
      if(VMReqTasks.FirstOrDefault() != null)
        return true;
      return false;
    }

  }

}