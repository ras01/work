﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.TestSolution.DocumentKind;

namespace centrvd.TestSolution
{
  partial class DocumentKindFilteringServerHandler<T>
  {

    public override IQueryable<T> Filtering(IQueryable<T> query, Sungero.Domain.FilteringEventArgs e)
    {
      query = base.Filtering(query, e);
      if(Sungero.Company.Employees.Current.Department.BusinessUnit.Name.Equals(TestSolution.Constants.Module.ivanovOrgName))
      {
        return query.Where(t => t.Name.Equals(TestSolution.Constants.Module.purchaseDocKind) == false &
                                t.Name.Equals(TestSolution.Constants.Module.mountingDocKind) == false);
      }
      if(Sungero.Company.Employees.Current.Department.BusinessUnit.Name.Equals(TestSolution.Constants.Module.techServiceName))
      {
        return query.Where(t => t.Name.Equals(TestSolution.Constants.Module.cnDocKind) == false);      
      }
      return query;
    }
  }

}