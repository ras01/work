﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.Tender.CommitteeDecision;

namespace centrvd.Tender
{
  partial class CommitteeDecisionServerHandlers
  {

    public override void AfterSave(Sungero.Domain.AfterSaveEventArgs e)
    {
      //foreach (var recepient in _obj.Department.RecipientLinks)
      //{
      //  if (!_obj.AccessRights.CanRead(recepient.Member))
      //    _obj.AccessRights.Grant(recepient.Member, DefaultAccessRightsTypes.Read);
      //}
      //_obj.AccessRights.Save();
    }

    public override void BeforeSaveHistory(Sungero.Domain.HistoryEventArgs e)
    {
      var isUpdateAction = e.Action == Sungero.CoreEntities.History.Action.Update;
      if (!isUpdateAction)
        return;
      
      var properties = _obj.State.Properties.Where(p =>
                                                   p == _obj.State.Properties.RegNumber ||
                                                   p == _obj.State.Properties.DocDate ||
                                                   p == _obj.State.Properties.TenderSubject ||
                                                   p == _obj.State.Properties.BeginDate ||
                                                   p == _obj.State.Properties.Validity);
      
      var propertiesType = CommitteeDecisions.Info.Properties.GetType();
      var objType = _obj.GetType();
      var operation = new Enumeration("SDChange");

      foreach (var property in properties)
      {
        var isChanged = (property as Sungero.Domain.Shared.IPropertyState).IsChanged;
        if (isChanged)
        {
          var propertyName = (property as Sungero.Domain.Shared.PropertyStateBase).PropertyName;
          var propertyInfo = propertiesType.GetProperty(propertyName).GetValue(CommitteeDecisions.Info.Properties);
          var name = propertyInfo.GetType().GetProperty("LocalizedName").GetValue(propertyInfo);
          
          var newValue = objType.GetProperty(propertyName).GetValue(_obj);
          
          var oldValue = property.GetType().GetProperty("OriginalValue").GetValue(property);
          
          if (newValue == oldValue ||
              newValue != null && oldValue != null && Equals(newValue.ToString(), oldValue.ToString()) ||
              newValue != null && oldValue == null && string.IsNullOrEmpty(newValue.ToString()) ||
              oldValue != null && newValue == null && string.IsNullOrEmpty(oldValue.ToString()))
            continue;

          var comment = string.Format("{0}. Новое значение: {1}. Прежнее значение: {2}", name, newValue, oldValue);
          e.Write(operation, null, comment);
        }
      }
    }
  }

}