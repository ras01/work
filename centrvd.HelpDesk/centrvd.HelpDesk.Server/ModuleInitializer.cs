﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace centrvd.HelpDesk.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      Requests.AccessRights.Grant(Roles.AllUsers, DefaultAccessRightsTypes.Read);
      RequestKinds.AccessRights.Grant(Roles.Administrators, DefaultAccessRightsTypes.FullAccess);
      InitializationLogger.Debug("Выданы права на просмотр всех обращений");
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentType("Приложение к обращению",AddendumInternalDocument.ClassTypeGuid,Sungero.Docflow.DocumentType.DocumentFlow.Inner,true);
    }
  }
}
