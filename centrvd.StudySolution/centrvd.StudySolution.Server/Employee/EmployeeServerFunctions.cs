﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.StudySolution.Employee;

namespace centrvd.StudySolution.Server
{
  partial class EmployeeFunctions
  {

    /// <summary>
    /// Получить все внутренние обращения выбранного сотрудника.
    /// </summary>
    /// <returns>Список внутренних обращений.</returns>
    [Remote(IsPure=true)]
    public IQueryable<centrvd.HelpDesk.IInternalRequest> GetEmployeeRequest()
    {
      return centrvd.HelpDesk.InternalRequests.GetAll(r => Equals(r.Author, _obj));
    }

  }
}