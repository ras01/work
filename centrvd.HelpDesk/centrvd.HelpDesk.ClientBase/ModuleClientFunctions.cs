﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace centrvd.HelpDesk.Client
{
  public class ModuleFunctions
  {

    /// <summary>
    ///Поиск обращений по организации
    /// </summary>
    public virtual void SearchRequestByCompany()
    {
      var dialog=Dialogs.CreateInputDialog("Поиск по организации");
      var company=dialog.AddSelect("Выберите организацию",true, Sungero.Parties.Companies.Null);
      if(dialog.Show()==DialogButtons.Ok)
      {
        var req=ExternalRequests.Get(r=>Equals(r.Company,company.Value));
        req.ShowModal();
      }
    }

    /// <summary>
    ///Поиск обращений по номеру
    /// </summary>
    public virtual void SearchRequestByNumber()
    {
      var dialog=Dialogs.CreateInputDialog("Поиск по номеру обращения");
      var number=dialog.AddString("Введите номер обращения",true);
      if(dialog.Show()==DialogButtons.Ok)
      {
        var req=Requests.Get().Where(t=>t.Number.Value==Convert.ToInt32(number.Value));
        req.ShowModal();
      }
    }


    /// <summary>
    /// Создать и отобразить карточку внешнего обращения.
    /// </summary>
    public virtual void CreateExternalRequest()
    {
      Functions.Module.Remote.CreateInternalRequest().Show();
    }

    /// <summary>
    /// Создать и отобразить карточку внутреннего обращения.
    /// </summary>
    public virtual void CreateInternalRequest()
    {
      Functions.Module.Remote.CreateInternalRequest().Show();
    }

  }
}