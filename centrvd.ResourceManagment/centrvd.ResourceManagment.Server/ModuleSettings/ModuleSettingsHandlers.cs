﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.ResourceManagment.ModuleSettings;

namespace centrvd.ResourceManagment
{
  partial class ModuleSettingsServerHandlers
  {

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      _obj.Name=Constants.ModuleSettings.Name;
      _obj.CheckFreq=Constants.ModuleSettings.FreqCheck;
      _obj.TaskDeadlineAct=Constants.ModuleSettings.TaskDeadlineAct;
      _obj.TaskDeadlineGive=Constants.ModuleSettings.TaskDeadlineGive;
    }
  }

}