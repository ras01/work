﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace centrvd.Tender.Server
{
  public class ModuleJobs
  {

    /// <summary>
    /// Уведомление об окончании действия
    /// </summary>
    public virtual void TimeNotification()
    {
      var Decisions=CommitteeDecisions.GetAll().Where(r=>Calendar.Today.AddDays(r.Notify.Value).CompareTo(r.OperatesOn.Value)==0);
      foreach (var element in Decisions)
      {
        // Список лиц роли Ответственный за тендеры
        var TRRole=Roles.GetAll(r=>r.Sid==Constants.Module.RoleGUID.TendersResponsible).FirstOrDefault();
        var users=Roles.GetAllUsersInGroup(TRRole);
        List<IRecipient> perfomersList=new List<IRecipient>();
        foreach (var user in users)
        {
          perfomersList.Add(Sungero.Company.Employees.As(user));
        }
        // Добавить руководителя подразделения инициатора в уведомление
        perfomersList.Add(element.Department.Manager);
        
        IRecipient [] perfomers=perfomersList.ToArray<IRecipient>();
        // Создать и стартовать задачу.
        var theme="Срок действия тендера "+ element.RegNumber + " подходит к концу";
        var task= Sungero.Workflow.SimpleTasks.CreateWithNotices(theme, perfomers);
        task.ActiveText = "Срок действия кончается " +Calendar.Now.AddDays(element.Notify.Value).ToString() + " числа.";
        task.Start();
      }
    }

    /// <summary>
    /// Перевод тендеров в устаревшие
    /// </summary>
    public virtual void SetOldDecision()
    {
      var Decisions=CommitteeDecisions.GetAll().Where(r=>r.OperatesOn.Value.CompareTo(Calendar.Now)<0);
      foreach (var element in Decisions)
      {
        element.Condition=centrvd.Tender.CommitteeDecision.Condition.UnActive;
        element.Save();
      }
    }

  }
}