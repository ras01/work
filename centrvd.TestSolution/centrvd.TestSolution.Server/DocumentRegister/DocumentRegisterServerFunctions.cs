﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.TestSolution.DocumentRegister;

namespace centrvd.TestSolution.Server
{
  partial class DocumentRegisterFunctions
  {  

    /// <summary>
    /// Получить следующий регистрационный номер.
    /// </summary>
    /// <param name="date">Дата регистрации.</param>
    /// <param name="leadDocumentId">ID ведущего документа.</param>
    /// <param name="document">Документ.</param>
    /// <param name="leadingDocumentNumber">Номер ведущего документа.</param>
    /// <param name="departmentId">ИД подразделения.</param>
    /// <param name="businessUnitId">ID НОР.</param>
    /// <param name="caseFileIndex">Индекс дела.</param>
    /// <param name="docKindCode">Код вида документа.</param>
    /// <param name="indexLeadingSymbol">Ведущий символ индекса.</param>
    /// <returns>Регистрационный номер.</returns>
    [Remote(IsPure = true)]
    public override string GetNextNumber(DateTime date, int leadDocumentId, Sungero.Docflow.IOfficialDocument document, string leadingDocumentNumber,
                                  int departmentId, int businessUnitId, string caseFileIndex, string docKindCode, string indexLeadingSymbol)
    {
      var number = base.GetNextNumber(date, leadDocumentId, document, leadingDocumentNumber, departmentId, businessUnitId, caseFileIndex, docKindCode, indexLeadingSymbol);
      
      #region CUSTOM
      if (document != null)
        number = ChangeCustomElementNumber(document, number);
      #endregion
      
      return number;
    }
    /// <summary>
    /// Заменить кастомные элементы номера.
    /// </summary>
    /// <param name="document">Id документа.</param>
    /// <param name="number">Регистрационный номер.</param>
    /// <returns>Регистрационный номер.</returns>
    [Public]
    public static string ChangeCustomElementNumber(object document, string number)
    {
      if (number.IndexOf(DocumentRegisters.Resources.IDOrg) > -1)
      {
        var selfOrg = Sungero.Company.BusinessUnits.GetAll().FirstOrDefault();
        if (selfOrg != null)
        {
          number = number.Replace(DocumentRegisters.Resources.IDOrg, selfOrg.Id.ToString());
        }
      }
      
      return number;
    }
  }
}