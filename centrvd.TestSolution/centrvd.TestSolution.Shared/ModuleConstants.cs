﻿using System;
using Sungero.Core;

namespace centrvd.TestSolution.Constants
{
  public static class Module
  {
    public const string ivanovOrgName = "ИП Иванов";
    public const string techServiceName = "ООО ТехСервис";
    public const string subcontractDocKind = "Договор субподряда";
    public const string cnDocKind = "Договор ГПХ";
    public const string purchaseDocKind = "Договор закупки";
    public const string mountingDocKind = "Договор на монтаж";
  }
}