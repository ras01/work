﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using centrvd.StudySolution.ApprovalTask;

namespace centrvd.StudySolution.Server
{
  partial class ApprovalTaskRouteHandlers
  {

    public override void CompleteAssignment6(Sungero.Docflow.IApprovalAssignment assignment, Sungero.Docflow.Server.ApprovalAssignmentArguments e)
    {
      var approvalAssigment=centrvd.StudySolution.ApprovalAssignments.As(assignment);
      if(approvalAssigment.ApprovedRemark==true)
      {
        var task = Sungero.Workflow.SimpleTasks.CreateAsSubtask(_obj);
        task.Subject = "Уведомление о согласовании с замечанием";
        task.AssignmentType = Sungero.Workflow.SimpleTask.AssignmentType.Notice;
        task.RouteSteps.AddNew().Performer = _obj.Author;
        task.Start();
      }
    }
  }


}