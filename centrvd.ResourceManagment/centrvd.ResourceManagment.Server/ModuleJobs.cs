﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace centrvd.ResourceManagment.Server
{
  public class ModuleJobs
  {

    /// <summary>
    /// ФП предназначен для рассылки задач на актуализацию состояния ВМ.
    /// </summary>
    public virtual void ExtensionJob()
    {
      var weekFreq = Convert.ToDouble(ModuleSettingses.GetAll().FirstOrDefault().CheckFreq)*7;
      var VMs = VirtualMachines.GetAll().Where(r => r.LastCheckDate.Value.AddDays(weekFreq) <= Calendar.Now);
      foreach(var vm in VMs)
      {
        var vmAdmin = Roles.GetAll().Where(n => n.Sid == Constants.Module.RoleGUID.VMAdmin).FirstOrDefault();
        var task = VMActualizationTasks.Create();
        task.Subject = "Актуализация состояния ВМ " + vm.Name + " от " + Calendar.Now + ".";
        task.AttachmentGroup.VirtualMachines.Add(vm);
        task.To = vm.Responsible;
        task.Text = ModuleSettingses.GetAll().FirstOrDefault().TaskText;
        task.Start();
        
      }
    }

  }
}