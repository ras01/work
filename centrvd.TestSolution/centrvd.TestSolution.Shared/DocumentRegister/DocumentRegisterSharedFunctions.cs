﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.TestSolution.DocumentRegister;
using System.Text.RegularExpressions;

namespace centrvd.TestSolution.Shared
{
  partial class DocumentRegisterFunctions
  {
 /// <summary>
    /// Генерировать префикс и постфикс регистрационного номера документа.
    /// </summary>
    /// <param name="date">Дата.</param>
    /// <param name="leadingDocumentNumber">Ведущий документ.</param>
    /// <param name="departmentCode">Код подразделения.</param>
    /// <param name="businessUnitCode">Код нашей организации.</param>
    /// <param name="caseFileIndex">Индекс дела.</param>
    /// <param name="docKindCode">Код вида документа.</param>
    /// <param name="counterpartyCode">Код контрагента.</param>
    /// <param name="counterpartyCodeIsMetasymbol">Признак того, что код контрагента нужен в виде метасимвола.</param>
    /// <returns>Сгенерированный регистрационный номер.</returns>
    public override Sungero.Docflow.Structures.DocumentRegister.RegistrationNumberParts GenerateRegistrationNumberPrefixAndPostfix(DateTime date, string leadingDocumentNumber,
                                                                                                                  string departmentCode, string businessUnitCode,
                                                                                                                  string caseFileIndex, string docKindCode,
                                                                                                                  string counterpartyCode, bool counterpartyCodeIsMetasymbol)
    {
      var prefix = string.Empty;
      var postfix = string.Empty;
      var numberElement = string.Empty;
      var orderedNumberFormatItems = _obj.NumberFormatItems.OrderBy(f => f.Number);
      foreach (var element in orderedNumberFormatItems)
      {
        if (element.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.Number)
        {
          prefix = numberElement;
          numberElement = string.Empty;
        }
        else if (element.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.Log)
          numberElement += _obj.Index;
        else if (element.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.RegistrPlace && _obj.RegistrationGroup != null)
          numberElement += _obj.RegistrationGroup.Index;
        else if (element.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.Year2Place)
          numberElement += date.ToString("yy");
        else if (element.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.Year4Place)
          numberElement += date.ToString("yyyy");
        else if (element.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.Month)
          numberElement += date.ToString("MM");
        else if (element.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.Quarter)
          numberElement += ToQuarterString(date);
        else if (element.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.Day)
          numberElement += date.ToString("dd");
        else if (element.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.LeadingNumber)
          numberElement += leadingDocumentNumber;
        else if (element.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.DepartmentCode)
          numberElement += departmentCode;
        else if (element.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.BUCode)
          numberElement += businessUnitCode;
        else if (element.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.CaseFile)
          numberElement += caseFileIndex;
        else if (element.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.DocKindCode)
          numberElement += docKindCode;
        else if (element.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.CPartyCode && !counterpartyCodeIsMetasymbol)
          numberElement += counterpartyCode;
        else if (element.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.CPartyCode && counterpartyCodeIsMetasymbol)
          numberElement += DocumentRegisters.Resources.NumberFormatCounterpartyCode;
        else
        #region CUSTOM
          if (element.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.IDOrg)
            numberElement += DocumentRegisters.Resources.IDOrg;
        #endregion
        // Не добавлять разделитель, для пустого кода контрагента.
        if (string.IsNullOrEmpty(counterpartyCode) || counterpartyCodeIsMetasymbol)
        {
          // Разделитель после пустого кода контрагента.
          if (element.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.CPartyCode)
            continue;
          
          // Разделитель до кода контрагента, если код контрагента последний в номере.
          var nextElement = orderedNumberFormatItems.Where(f => f.Number > element.Number).FirstOrDefault();
          var lastElement = orderedNumberFormatItems.LastOrDefault();
          if (nextElement != null && nextElement.Element == TestSolution.DocumentRegisterNumberFormatItems.Element.CPartyCode &&
              lastElement != null && lastElement.Number == nextElement.Number)
            continue;
        }
        
        // Добавить разделитель.
        numberElement += element.Separator;
      }
      
      postfix = numberElement;
      return Sungero.Docflow.Structures.DocumentRegister.RegistrationNumberParts.Create(prefix, postfix);
    }
        /// <summary>
    /// Проверить регистрационный номер на валидность.
    /// </summary>
    /// <param name="registrationDate">Дата регистрации.</param>
    /// <param name="registrationNumber">Номер регистрации.</param>
    /// <param name="departmentCode">Код подразделения.</param>
    /// <param name="businessUnitCode">Код нашей организации.</param>
    /// <param name="caseFileIndex">Индекс дела.</param>
    /// <param name="docKindCode">Код вида документа.</param>
    /// <param name="counterpartyCode">Код контрагента.</param>
    /// <param name="leadDocNumber">Номер ведущего документа.</param>
    /// <param name="searchCorrectingPostfix">Искать корректировочный постфикс.</param>
    /// <returns>Сообщение об ошибке. Пустая строка, если номер соответствует журналу.</returns>
    /// <remarks>Пример: 5/1-П/2020, где 5 - порядковый номер, П - индекс журнала, 2020 - год, /1 - корректировочный постфикс.</remarks>
    public override string CheckRegistrationNumberFormat(DateTime? registrationDate, string registrationNumber,
                                                        string departmentCode, string businessUnitCode, string caseFileIndex, string docKindCode, string counterpartyCode,
                                                        string leadDocNumber, bool searchCorrectingPostfix)
    {
      if (string.IsNullOrWhiteSpace(registrationNumber))
        return DocumentRegisters.Resources.EnterRegistrationNumber;
      
      // Регулярное выражение для рег. индекса.
      // "([0-9]+)" определяет, где искать индекс в номере.
      // "([\.\/-][0-9]+)?" определяет, где искать корректировочный постфикс в номере.
      // Пустые скобки в выражении @"([0-9]+)()" означают корректировочный постфикс,
      // чтобы количество групп в результате регулярного выражения было всегда одинаковым, независимо от того, нужно искать корректировочный постфикс или нет.
      var indexTemplate = searchCorrectingPostfix ? @"([0-9]+)([\.\/-][0-9]+)?" : @"([0-9]+)()";
      
      // Перед проверкой правильности формата дополнительно проверить наличие непечатных символов в строке ("\s").
      if (Regex.IsMatch(registrationNumber, @"\s"))
        return DocumentRegisters.Resources.NoSpaces;
      
      if (!GetRegexMatchFromRegistrationNumber(_obj, registrationDate ?? Calendar.UserToday, registrationNumber, indexTemplate,
                                               departmentCode, businessUnitCode, caseFileIndex, docKindCode, counterpartyCode, leadDocNumber,
                                               string.Empty, string.Empty)
          .Success)
      {
        // Шаблон номера, состоящий из символов "*".
        var numberTemplate = string.Concat(Enumerable.Repeat("*", _obj.NumberOfDigitsInNumber.Value));
        var example = base.GenerateRegistrationNumber(registrationDate.Value, numberTemplate, departmentCode, businessUnitCode, caseFileIndex, docKindCode, counterpartyCode, "0");
        return Sungero.Docflow.Resources.RegistrationNumberNotMatchFormatFormat(example);
      }
      
      return string.Empty;
    }
        /// <summary>
    /// Получить сравнение рег.номера с шаблоном.
    /// </summary>
    /// <param name="documentRegister">Журнал.</param>
    /// <param name="date">Дата.</param>
    /// <param name="registrationNumber">Рег. номер.</param>
    /// <param name="indexTemplate">Шаблон номера.</param>
    /// <param name="departmentCode">Код подразделения.</param>
    /// <param name="businessUnitCode">Код нашей организации.</param>
    /// <param name="caseFileIndex">Индекс дела.</param>
    /// <param name="docKindCode">Код вида документа.</param>
    /// <param name="counterpartyCode">Код контрагента.</param>
    /// <param name="leadDocNumber">Номер ведущего документа.</param>
    /// <param name="numberPostfix">Постфикс номера.</param>
    /// <param name="additionalPrefix">Дополнительный префикс номера.</param>
    /// <returns>Индекс.</returns>
    internal static Match GetRegexMatchFromRegistrationNumber(IDocumentRegister documentRegister, DateTime date, string registrationNumber,
                                                              string indexTemplate, string departmentCode, string businessUnitCode,
                                                              string caseFileIndex, string docKindCode, string counterpartyCode, string leadDocNumber,
                                                              string numberPostfix, string additionalPrefix)
    {
      var prefixAndPostfix = Functions.DocumentRegister.GenerateRegistrationNumberPrefixAndPostfix(documentRegister, date, leadDocNumber, departmentCode,
                                                                                                   businessUnitCode, caseFileIndex, docKindCode, counterpartyCode, true);
      var template = string.Format("{0}{1}{2}{3}", Regex.Escape(prefixAndPostfix.Prefix), 
                                                   indexTemplate, 
                                                   Regex.Escape(prefixAndPostfix.Postfix), 
                                                   numberPostfix);
      
      // Заменить метасимвол для кода контрагента на соответствующее регулярное выражение.
      var metaCounterpartyCode = Regex.Escape(DocumentRegisters.Resources.NumberFormatCounterpartyCode);
      template = template.Replace(metaCounterpartyCode,  Sungero.Docflow.Constants.DocumentRegister.CounterpartyCodeRegex);
   
      #region CUSTOM
      // Заменить метасимвол для ид нашей организации на соответствующее регулярное выражение.
      var metaRequestKindCode = Regex.Escape(DocumentRegisters.Resources.IDOrg);
      template = template.Replace(metaRequestKindCode, Sungero.Docflow.Constants.DocumentRegister.CounterpartyCodeRegex);
      #endregion
      
      // Совпадение в начале строки.
      var numberTemplate = string.Format("^{0}", template);
      var match = Regex.Match(registrationNumber, numberTemplate);
      if (match.Success)
        return match;
      
      // Совпадение в конце строки.
      numberTemplate = string.Format("{0}{1}$", additionalPrefix, template);
      return Regex.Match(registrationNumber, numberTemplate);
    }
  }
}