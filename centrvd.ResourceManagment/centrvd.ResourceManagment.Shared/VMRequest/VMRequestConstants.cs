﻿using System;
using Sungero.Core;

namespace centrvd.ResourceManagment.Constants
{
  public static class VMRequest
  {
    public const string taskSubject = "Запрос на предоставление ВМ";
    public const string initName = "<Имя запроса сформируется автоматически>";
  }
}