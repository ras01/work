﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace centrvd.Tender.Server
{
  public partial class ModuleInitializer
  {

    public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
    {
      CreateDocumentTypes();
      CreateDocumentKinds();
      CreateRoles();
      GrantRights();
    }

    /// <summary>
    /// Создание типа документа "Документы по решению ТК" в служебном справочнике "Типы документов".
    /// </summary>
    public static void CreateDocumentTypes()
    {
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentType("Документы по решению ТК",
                                                                              DocumentByTCDecision.ClassTypeGuid,
                                                                              Sungero.Docflow.DocumentType.DocumentFlow.Inner,
                                                                              false);
    }
    
    /// <summary>
    /// Создание видов документа Документы по решению ТК.
    /// </summary>
    public static void CreateDocumentKinds()

    {
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentKind("Документы по решению ТК", "Документы по решению ТК",
                                                                              Sungero.Docflow.DocumentKind.NumberingType.Numerable,
                                                                              Sungero.Docflow.DocumentType.DocumentFlow.Inner,
                                                                              true, false, DocumentByTCDecision.ClassTypeGuid,
                                                                              null,
                                                                              Constants.Module.DocumentByDecisionKind);
    }

    /// <summary>
    /// Создание ролей
    /// </summary>
    public static void CreateRoles()
    {
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateRole("Генеральный директор",
                                                                      "Сотрудник может просматривать карточки и документы блока Закупки/ тендеры + вкладки Закупки/ тендеры",
                                                                      Constants.Module.RoleGUID.GeneralManager);
      
      Sungero.Docflow.PublicInitializationFunctions.Module.CreateRole("Ответственный за тендеры",
                                                                      "Сотруднику доступны все действия с карточками и документами",
                                                                      Constants.Module.RoleGUID.TendersResponsible);
    }
    
    /// <summary>
    /// Выдача прав
    /// </summary>
    public static void GrantRights()
    {
      var GeneralManager=Roles.GetAll().Where(n=>n.Sid==Constants.Module.RoleGUID.GeneralManager).FirstOrDefault();
      DocumentByTCDecisions.AccessRights.Grant(GeneralManager, DefaultAccessRightsTypes.Read);
      CommitteeDecisions.AccessRights.Grant(GeneralManager, DefaultAccessRightsTypes.Read);
      
      var TendersResponsible=Roles.GetAll().Where(n=>n.Sid==Constants.Module.RoleGUID.TendersResponsible).FirstOrDefault();
      DocumentByTCDecisions.AccessRights.Grant(TendersResponsible, DefaultAccessRightsTypes.FullAccess);
      CommitteeDecisions.AccessRights.Grant(TendersResponsible, DefaultAccessRightsTypes.FullAccess);
        
      DocumentByTCDecisions.AccessRights.Save();
      CommitteeDecisions.AccessRights.Save();
    }
  }
}