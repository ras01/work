﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.TestSolution.DocumentRegister;

namespace centrvd.TestSolution
{
  partial class DocumentRegisterNumberFormatItemsClientHandlers
  {

    public override IEnumerable<Enumeration> NumberFormatItemsElementFiltering(IEnumerable<Enumeration> query)
    {
      query = base.NumberFormatItemsElementFiltering(query);
      
      #region CUSTOM
      var excludeIdOrg = true;
      var documentRegisters = Sungero.Docflow.PublicFunctions.RegistrationSetting.GetByDocumentRegister(Sungero.Docflow.DocumentRegisters.As(_obj.DocumentRegister));      
      var documentKinds = documentRegisters.SelectMany(r => r.DocumentKinds.Select(d => d.DocumentKind));
      
      if (documentKinds.Any(dk => dk.DocumentType.DocumentTypeGuid == Sungero.Docflow.Constants.Module.MemoTypeGuid))
        excludeIdOrg = false;
      
       //Исключить элемент "ИД нашей орг" для всех ТД, кроме СЗ
      if (excludeIdOrg)
        query = query.Where(e => e != centrvd.TestSolution.DocumentRegisterNumberFormatItems.Element.IDOrg);
      #endregion
      return query;
    }
  }


  partial class DocumentRegisterClientHandlers
  {

  }
}