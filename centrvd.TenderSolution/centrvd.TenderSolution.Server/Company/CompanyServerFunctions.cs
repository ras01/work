﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.TenderSolution.Company;

namespace centrvd.TenderSolution.Server
{
  partial class CompanyFunctions
  {

    /// <summary>
    /// Получить закупки организации
    /// </summary>
    /// <returns>Список закупок</returns>
    [Remote(IsPure=true)]
    public IQueryable<centrvd.Tender.ICommitteeDecision> GetCommitteeDecisions()
    {
      return centrvd.Tender.CommitteeDecisions.GetAll().Where(r=>r.MainSupplierCollection.Where(t=>t.MainSupplier==_obj)!=null ||
                                                              r.ReserveSupplierCollection.Where(t=>t.ReserveSupplier==_obj)!=null);
    }
  }
}