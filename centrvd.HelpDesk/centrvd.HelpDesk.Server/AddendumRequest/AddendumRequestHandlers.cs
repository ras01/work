using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.HelpDesk.AddendumRequest;

namespace centrvd.HelpDesk
{
  partial class AddendumRequestServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      if(!_obj.AccessRights.IsGranted(DefaultAccessRightsTypes.Change,_obj.Request.Responsible))
        _obj.AccessRights.Grant(_obj.Request.Responsible,DefaultAccessRightsTypes.Change);
    }
  }

}