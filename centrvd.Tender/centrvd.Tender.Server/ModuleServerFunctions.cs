﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace centrvd.Tender.Server
{
  public class ModuleFunctions
  {
    /// <summary>
    /// Создать тендер.
    /// </summary>
    /// <returns>Созданный тендер.</returns>
    [Remote]
    public static ICommitteeDecision CreateCommitteeDecision()
    {
      return CommitteeDecisions.Create();
    }

  }
}