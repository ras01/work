﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.HelpDesk.HelpDeskTask;

namespace centrvd.HelpDesk
{
  partial class HelpDeskTaskServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      if(_obj.MaxDeadline!=null && _obj.MaxDeadline<=Calendar.Now.AddHours(1))
        e.AddError("Нельзя создать задачу сроком на выполнение меньше 1 часа");
      if(_obj.ActiveText=="")
        e.AddError("Текст задачи обязательно для заполнения");
      if(_obj.State.Equals(Sungero.Workflow.Task.Status.Draft))
        _obj.State.Attachments.AttachmentGroupRequest.IsVisible=false;
    }

    public override void Created(Sungero.Domain.CreatedEventArgs e)
    {
      _obj.MaxDeadline=Calendar.Now.AddDays(2);
      _obj.Subject="<Тема будет сформирована автоматически >";
    }
  }

}