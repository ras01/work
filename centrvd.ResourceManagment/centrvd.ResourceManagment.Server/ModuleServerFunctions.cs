﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace centrvd.ResourceManagment.Server
{
  public class ModuleFunctions
  {

    /// <summary>
    /// Создать запрос на создание ВМ.
    /// </summary>
    /// <returns>Созданный запрос.</returns>
    [Remote]
    public static IVMRequest CreateIVMRequest()
    {
      return VMRequests.Create();
    }
    
    /// <summary>
    /// Создать карточку Виртуальных машин из CSV по пути.
    /// </summary>
    [Remote]
    public static void CreateVMFromCSV(string path)
    {
      List<Structures.Module.CSVData> CSV_Struct = new List<Structures.Module.CSVData>();
      CSV_Struct = ReadFile(path);
      
      for (int i = 0; i <= CSV_Struct.Count-1; i++)
      {
        var VM=VirtualMachines.Create();
        VM.Name=CSV_Struct[i].Name;
        VM.ServerName=CSV_Struct[i].Host;
        VM.Memory=CSV_Struct[i].UsedSpace;
        VM.OS=CSV_Struct[i].GuestOS;
        VM.IP=CSV_Struct[i].IP;
        VM.DNS=CSV_Struct[i].DNS;
        VM.Status=centrvd.ResourceManagment.VirtualMachine.Status.Active;
        VM.VMCycle=centrvd.ResourceManagment.VirtualMachine.VMCycle.InWork;
        VM.LastCheckDate=Calendar.Now;
        VM.State.Properties.Project.IsRequired=false;
        VM.State.Properties.Responsible.IsRequired=false;
        VM.Save();
      }
    } 
    
    /// <summary>
    /// Считывание данных из CSV в лист структуры.
    /// </summary>
    /// <returns>Лист структуры.</returns>
    public static List<Structures.Module.CSVData> ReadFile(string filename)
    {
      List<Structures.Module.CSVData> res = new List<Structures.Module.CSVData>();
      using (StreamReader sr = new StreamReader(filename))
      {
        string line;
        while ((line = sr.ReadLine()) != null)
        {
          Structures.Module.CSVData p = new Structures.Module.CSVData();
          string[] parts=line.Split(',');  //Разделитель в CVS файле.
          p.Name=parts[0];
          p.Host=parts[1];
          p.UsedSpace=Convert.ToInt32(parts[2]);
          p.GuestOS=parts[3];
          p.IP=parts[4];
          p.DNS=parts[5];
          res.Add(p);
        }
      }
      
      return res;
    }
  }
  
}