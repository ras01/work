﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using centrvd.HelpDesk.Request;

namespace centrvd.HelpDesk.Client
{
  partial class RequestActions
  {

    public virtual void RequestDocuments(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      var reqDocuments=AddendumRequests.GetAll(r=>r.Request==_obj);
      reqDocuments.Show();
    }

    public virtual bool CanRequestDocuments(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return _obj.State.IsInserted==true?false:true;
    }

    public virtual void CreateDocument(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      Functions.Request.Remote.CreateAddendumRequest(_obj).Show();
    }

    public virtual bool CanCreateDocument(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return _obj.State.IsInserted==true?false:true;
    }

    public virtual void OpenRequest(Sungero.Domain.Client.ExecuteActionArgs e)
    {
      _obj.LifeCycle=LifeCycle.InWork;
      _obj.State.IsEnabled=true;
      _obj.ClosedDate=null;
    }

    public virtual bool CanOpenRequest(Sungero.Domain.Client.CanExecuteActionArgs e)
    {
      return _obj.LifeCycle.Equals(LifeCycle.Closed);
    }

  }

}